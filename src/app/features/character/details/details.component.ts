import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Character } from 'src/app/models';
import { RickMortyService } from 'src/app/services/rick-morty.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public character: Character) {
    console.log(character);
  }

  getThumbnail() {
    return {
      'background-image': `url('${this.character.image}')`,
      'background-size': 'cover'
    }
  }

}
