import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Character } from 'src/app/models';
import { RickMortyService } from 'src/app/services/rick-morty.service';
import { DetailsComponent } from '../details/details.component';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent {

  characters: Character[] = [];
  length = 0;
  pageSize = 20;

  constructor(private service: RickMortyService,
    public dialog: MatDialog) {
    this.getPage()
  }

  getStyle(status: string) {
    let color = '';
    if (status === 'Alive') {
      color = 'green';
    } else if (status === 'Dead') {
      color = 'red'
    }
    return { color, }
  }

  pageEvent(event: PageEvent) {
    this.getPage(event.pageIndex + 1);
  }

  private getPage(page: number = 0) {
    this.service.allCharacters(page)
      .subscribe(res => {
        this.characters = res.results;
        this.length = res.info.count;
      });
  }

  showMore(character: Character) {
    this.dialog.open(DetailsComponent, { data: { ...character } });
  }

}
