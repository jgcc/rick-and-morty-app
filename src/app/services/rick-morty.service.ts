import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { PaginatedResult, Character } from '../models';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class RickMortyService {

  private baseUrl = 'https://rickandmortyapi.com/api';

  private get endpoint(): string {
    return this.baseUrl + '/character';
  }

  constructor(private httpClient: HttpClient) { }

  allCharacters(page: number): Observable<PaginatedResult<Character>> {
    const params = this.generateParams(page);
    return this.httpClient.get<PaginatedResult<Character>>(this.endpoint, { params });
  }

  private generateParams(page: number): HttpParams {
    if (page == 0) {
      return new HttpParams();
    }
    return new HttpParams().set('page', page);
  }
}
