export interface PaginatedResult<T = any> {
    info: {
        count: number,
        pages: number,
        current: number,
        next: string,
        prev?: string,
    }
    results: T[]
}