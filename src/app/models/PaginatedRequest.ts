export interface PaginatedRequest {
    currentPage: number
}